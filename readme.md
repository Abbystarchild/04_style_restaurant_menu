Menu 1: 1. Applied appropriate fonts and sizes: a. Used ‘Lato’ at 18px throughout the body; b. Applied ‘Supermercado One’ at 45px to the restaurant name; c. Added bold and 20px each menu item name.

Menu1: 2. Used correct font colors: a. Applied #5d4037 to the restaurant name; b. Applied #6d4c41 to section headings; c. Applied “firebrick” or #b22222 to the item price.
erion is linked to a Learning OutcomeMenu1: 3. Section headings of the menu (e.g. “Starters”) are in small caps and the price of each item is in a large font.

Menu 1: 4. Added a separator between the columns (the "column-rule") and added a border around the address/footer. The separator and the border are 2px dashed rosybrown or #bc8f8f.

Menu1: 5. Covered the background with the ‘map-b.jpg’ image.

Menu 2: 1. Applied appropriate fonts and sizes a. Used ‘Oswald’ at 14px throughout the body; b. applied ‘Bungee Inline’ at 46px with a lighter weight to the restaurant name; c. Added bold and 16px to each menu item name.

Menu 2: 2. Each section header (e.g. "Starters") is in uppercase.

Menu 2: 3. There is a comma after each menu item before the description.

Menu 2: 4. The address/footer is right-aligned.

Menu 2: 5. There is a 2px thick black border around the starters and desserts sections of the menu.
